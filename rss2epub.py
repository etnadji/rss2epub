#!/usr/bin/python2
# -*- coding:Utf-8 -*-

"""
Rss2Epub

Make EPUB from a RSS feed using Pandoc.

Usage:
    rss2epub make <feed_url> <output_file> [--out-override] 
                  [--creator=<name>] [--entry-limit=<entry_number>] 
                  [--privacy] [--verbose]

Options:
  -h --help       Show this screen.
  --version       Show version.
"""

# Imports ===============================================================#

from __future__ import print_function

import os
import sys
import shutil
import tempfile

import feedparser

from docopt import docopt

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Fonctions =============================================================#

def _get_feed(feed,verbose,entry_limit):
    feed_url = feed
    feed_parsed = feedparser.parse(feed)

    try:
        title = feed_parsed["feed"]["title"].encode("utf-8")

    except KeyError:
        print ("There is no article entries in that feed or may be the feeds url lead to 404 error.")
        return False,False

    if verbose:
        print ("RSS feed:",feed_url)
        print ("RSS feed name:",title)

    entries = _get_entries(feed_parsed,title,verbose,entry_limit)

    return title,entries

def _get_entries(parsed_feed,title,verbose,entry_limit):
    if verbose:
        print ('Creating articles entries of "{0}"'.format(title))

    entries = []

    if verbose:
        entry_count = 1

    for ent in parsed_feed["entries"]:

        if entry_limit:
            if len(entries) >= entry_limit:
                break

        title = ent["title"].encode("utf-8")
        link  = ent["link"].encode("utf-8")

        entry = {
                "content":None,
                "link":link,
                "title":title,
                "date":None,
                }

        try:
            entry["content"] = ent["content"][0]["value"].encode("utf-8")
        except KeyError:
            try:
                entry["content"] = ent["summary"].encode("utf-8")
            except KeyError:
                pass

        try:
            entry["date"] = ent["published"].encode("utf-8")
        except KeyError:
            pass

        if verbose:
            if entry["date"] is None:
                print ("Entry {0}\n    {1}\n".format(entry_count,entry["title"]))
            else:
                print ("Entry {0} ( {1} ):\n    {2}\n".format(entry_count,entry["date"],entry["title"]))

            entry_count += 1

        entries.append(entry)

    return entries

def rss2epub(feed,epub_dest,**kwargs):
    verbose = False
    privacy = False
    creator = False
    override = False
    entry_limit = 0

    for key in kwargs:
        if key == "verbose":
            verbose = kwargs[key]
        if key == "override":
            override = kwargs[key]
        if key == "entry_number":
            entry_limit = kwargs[key]
        if key == "creator":
            creator = kwargs[key]
        if key == "privacy":
            privacy = kwargs[key]

    if not epub_dest.endswith(".epub"):
        epub_dest += ".epub"

    if os.path.exists(epub_dest):
        if override:
            os.remove(epub_dest)
        else:
            print("The {0} file already exists. Aborting.".format(epub_dest))
            sys.exit(1)

    title,entries = _get_feed(feed,verbose,entry_limit)

    if title and entries:

        final_html,epub_file = "final.html","final.epub"
        temp_folder = tempfile.mkdtemp(prefix='rss2epub_')
        os.chdir(temp_folder)

        epub_text = "<html>\n<head></head>\n<body>\n"
        epub_text += "<h1>{0}</h1>\n".format(title)

        with open("metadata.xml","w") as metadata:
            if creator:
                metadata.write("<dc:creator>{0}</dc:creator>\n".format(creator))
            else:
                metadata.write("<dc:creator>Rss2Epub</dc:creator>\n")

            metadata.write("<dc:title>{0}</dc:title>\n".format(title))
            metadata.write("<dc:source>{0}</dc:source>".format(feed))

        with open("style.css","w") as css:
            css.write("body { margin: 5%; text-align: justify; font-size: medium; }\n")
            css.write("code { font-family: monospace; }\n")
            css.write("h1 { text-align: left; }\n")
            css.write("h2 { text-align: left; }\n")
            css.write("h3 { text-align: left; }\n")
            css.write("h4 { text-align: left; }\n")
            css.write("h5 { text-align: left; }\n")
            css.write("h6 { text-align: left; }\n")
            css.write("h1.title {TEST}\n")
            css.write("h2.author { }\n")
            css.write("h3.date { }\n")
            css.write("ol.toc { padding: 0; margin-left: 1em; }\n")
            css.write("ol.toc li { list-style-type: none; margin: 0; padding: 0; }\n")
            css.write("p {text-align: justify;}\n")

        for e in entries:
            raw_entry,clean_entry = "raw.html","clean.html"

            with open(raw_entry,"w") as ef:
                try:
                    if e["link"]:
                        ef.write('<p">')
                        ef.write('<b>Link: </b>')
                        ef.write('<a href="{0}">{0}</a>'.format(e["link"]))
                        ef.write('</p>\n\n')

                    ef.write(e["content"])

                except TypeError:
                    pass

            os.system(
                "cat {0} | pandoc --from=html --to=markdown | pandoc --from=markdown --to=html > {1}".format(
                    raw_entry,clean_entry
                )
            )

            if privacy:
                os.system("shred {0}".format(raw_entry))
                os.system("shred -u -z {0}".format(raw_entry))
            else:
                os.remove(raw_entry)

            with open(clean_entry,"r") as ff:
                epub_text += "\n"
                epub_text += "<h2>{0}</h2>\n".format(e["title"])
                epub_text += "{0}\n".format(ff.read())
                epub_text += "<hr />"

            if privacy:
                os.system("shred {0}".format(raw_entry))
                os.system("shred -u -z {0}".format(raw_entry))
            else:
                os.remove(clean_entry)

        epub_text += "\n</body>\n</html>"

        with open(final_html,"w") as fin:
            fin.write(epub_text)

        os.system(
            "cat {0} | pandoc --from=html --to=epub -o {1} --epub-stylesheet=style.css --epub-metadata=metadata.xml".format(
                final_html,
                epub_file
            )
        )

        if privacy:
            for f in ["metadata.xml","style.css"]:
                os.system("shred {0}".format(f))
                os.system("shred -u -z {0}".format(f))
        else:
            for f in ["metadata.xml","style.css"]:
                os.remove(f)

        os.remove(final_html)

        shutil.move(epub_file,epub_dest)

        sys.exit(0)

    else:
        sys.exit(1)

# Programme =============================================================#

if __name__ == "__main__":
    arguments = docopt(__doc__, version='0.1')

    CURDIR = "{0}{1}".format(os.path.realpath(os.curdir),os.sep)

    if arguments["make"]:
        valid = True

    if valid:
        if arguments["--entry-limit"] is None:
            limit = 0
        else:
            limit = int(arguments["--entry-limit"])

        if arguments["--creator"] is None:
            creator = False
        else:
            creator = arguments["--creator"]

        if arguments["<feed_url>"] and arguments["<output_file>"]:

            if not os.sep in arguments["<output_file>"]:
                # No folder separator in the output_file path so we assume that the
                # output_file will be on the current (shell) path
                arguments["<output_file>"] = CURDIR + arguments["<output_file>"]

            rss2epub(
                arguments["<feed_url>"],
                arguments["<output_file>"],
                privacy=arguments["--privacy"],
                override=arguments["--out-override"],
                entry_number=limit,
                creator=creator,
                verbose=arguments["--verbose"]
            )

            sys.exit(0)
        else:
            sys.exit(1)

    else:
        sys.exit(1)

# Fin ===================================================================#
# vim:set shiftwidth=4 softtabstop=4:
