# rss2epub

## Requirements

- Python 2
- Python 2 modules:
  - feedparser module
  - docopt module

## Usage

```
rss2epub make <feed_url> <output_file> [--out-override] 
[--creator=<name>] [--entry-limit=<entry_number>] 
[--privacy] [--verbose]
```

### Examples

```bash
# shaarlo.epub is the output file

rss2epub.py make "http://shaarli.fr/?do=rss" "shaarlo"
```

### Arguments

#### out-override

Just override the existing output_file.

#### creator

Set an author in the EPUB metadatas.

#### entry-limit

If entry-limit is set to 50, rss2epub will only make an EPUB with the 50 most recent RSS entries.

#### privacy

Remove temporary files with shred.
